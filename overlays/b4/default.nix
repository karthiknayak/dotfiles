{ channels, ... }:

final: prev: {
  b4 = prev.b4.overrideAttrs (old: rec {
    src = prev.fetchFromGitHub {
      owner = "mricon";
      repo = "b4";
      rev = "master";
      sha256 = "sha256-ewWKZJVnCP5CsRaplYZ4n1lYnvjJ/JUezJoks93KqSU=";
    };
  });
}

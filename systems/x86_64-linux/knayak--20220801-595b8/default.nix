{
  lib,
  pkgs,
  config,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
{
  imports = [ ./hardware-configuration.nix ];

  batteries = {
    roles.common = enabled;
    roles.graphical = enabled;
    roles.work = enabled;
    roles.development = enabled;
  };

  services.openssh.enable = true;
  system.stateVersion = "24.11";
}

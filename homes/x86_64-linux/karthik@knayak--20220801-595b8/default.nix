{
  lib,
  pkgs,
  config,
  osConfig ? { },
  format ? "unknown",
  namespace,
  ...
}:
with lib.${namespace};
{
  batteries = {
    roles.common = enabled;
    roles.graphical = enabled;
    roles.development = enabled;
  };

  home.stateVersion = "24.11";
}

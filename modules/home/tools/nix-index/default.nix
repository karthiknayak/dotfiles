{
  lib,
  config,
  inputs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.tools.nix-index;
in {
  options.batteries.tools.nix-index = with types; {
    enable = mkBoolOpt false "Whether or not to nix index";
  };

  imports = with inputs; [
    nix-index-database.hmModules.nix-index
  ];

  config = mkIf cfg.enable {
    programs.nix-index = {
      enable = true;
      enableFishIntegration = true;
    };
    programs.nix-index-database.comma.enable = true;
  };
}

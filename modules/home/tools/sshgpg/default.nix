{
  lib,
  config,
  pkgs,
  namespace,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled;

  cfg = config.batteries.tools.sshgpg;
in
{
  options.batteries.tools.sshgpg = {
    enable = mkEnableOption "Enable SSH/GPG";
  };

  config = mkIf cfg.enable {
    programs.gpg.enable = true;
    services.gpg-agent = {
      enable = true;
      enableSshSupport = true;
      defaultCacheTtl = 86400;
      defaultCacheTtlSsh = 86400;
      maxCacheTtl = 86400;
      maxCacheTtlSsh = 86400;
      pinentryPackage = pkgs.pinentry-gtk2;
    };
  };
}

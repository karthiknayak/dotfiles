{
  lib,
  config,
  pkgs,
  namespace,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled;

  cfg = config.batteries.tools.git;
  user = config.batteries.user;
in
{
  options.batteries.tools.git = {
    enable = mkEnableOption "Git";
    userName = mkOpt types.str user.fullName "The name to configure git with.";
    userEmail = mkOpt types.str user.email "The email to configure git with.";
    signingKey = mkOpt types.str "3ED59F248E468C7F" "The key ID to sign commits with.";
    signByDefault = mkOpt types.bool true "Whether to sign commits by default.";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      b4
      lieer
      notmuch
    ];

    home.file.".config/git/b4.template" = {
      source = ./b4.template;
      force = true;
    };

    programs.git = {
      enable = true;
      inherit (cfg) userName userEmail;

      signing = {
        key = cfg.signingKey;
        inherit (cfg) signByDefault;
      };

      extraConfig = {
        b4 = {
          send-same-thread = "shallow";
          prep-cover-template = "${config.home.homeDirectory}/.config/git/b4.template";
        };

        color.ui = true;
        column.ui = "auto";

        core.excludesfile = "~/.config/git/ignore";

        diff = {
          colorMoved = "default";
          wsErrorHighlight = "all";
        };

        init = {
          defaultRefFormat = "reftable";
        };

        pull = {
          rebase = true;
        };

        push = {
          autoSetupRemote = true;
          default = "current";
        };

        rebase = {
          autosquash = true;
        };

        rerere = {
          enabled = true;
        };

        sendemail = {
          smtpEncryption = "tls";
          smtpServer = "smtp.gmail.com";
          smtpServerPort = 587;
          smtpUser = cfg.userEmail;
        };

        credential = {
          helper = "store";
        };
      };
    };
  };
}

{
  lib,
  config,
  pkgs,
  namespace,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled;

  cfg = config.batteries.tools.alacritty;
in
{
  options.batteries.tools.alacritty = {
    enable = mkEnableOption "Enable alacritty";
  };

  config = mkIf cfg.enable {
    programs.alacritty = {
      enable = true;

      settings = {
        window = {
          padding = {
            x = 30;
            y = 30;
          };
          decorations = "none";
        };

        selection = { save_to_clipboard = true; };

        mouse.bindings = [{
          mouse = "Right";
          action = "Paste";
        }];

        env = { TERM = "xterm-256color"; };

        # TODO: Font
        # font = {
        #   normal = {
        #     inherit (config.fontProfiles.monospace) family;
        #     style = "Regular";
        #   };
        #   bold = {
        #     inherit (config.fontProfiles.monospace) family;
        #     style = "Bold";
        #   };
        #   italic = {
        #     inherit (config.fontProfiles.monospace) family;
        #     style = "Italic";
        #   };
        #   size = 14.0;
        # };
      };
    };

  };
}

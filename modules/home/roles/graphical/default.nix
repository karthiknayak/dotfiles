{
  lib,
  config,
  pkgs,
  namespace,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled mkBoolOpt;

  cfg = config.batteries.roles.graphical;
in
{
  options.batteries.roles.graphical = {
    enable = mkBoolOpt false "Whether or not to enable graphical role.";
  };

  config = mkIf cfg.enable {
    home.sessionVariables = {
      MOZ_ENABLE_WAYLAND = 1;
      QT_QPA_PLATFORM = "wayland;xcb";
      LIBSEAT_BACKEND = "logind";
    };

    batteries = {
      desktop = {
        hyprland = enabled;
        styling = enabled;
        syncthing = enabled;
      };

      gui = {
        programs = enabled;
        music = enabled;

        social = {
          discord = enabled;
        };
      };

      browsers = {
        firefox = enabled;
      };

      editors = {
        libreoffice = enabled;
      };

      services.kdeconnect = enabled;
    };
  };
}

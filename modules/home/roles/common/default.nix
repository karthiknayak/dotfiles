{
  lib,
  config,
  pkgs,
  namespace,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled mkBoolOpt;

  # TODO https://github.com/snowfallorg/lib/issues/142
  cfg = config.batteries.roles.common;
in
{
  options.batteries.roles.common = {
    enable = mkBoolOpt false "Whether or not to enable common role.";
  };

  config = mkIf cfg.enable {
    batteries = {
      tools = {
        git = enabled;
        alacritty = enabled;
      };

      shells = {
        fish = enabled;
      };

      system.nix = enabled;
    };
  };
}

{
  lib,
  config,
  pkgs,
  namespace,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled mkBoolOpt;

  cfg = config.batteries.roles.development;
in
{
  options.batteries.roles.development = {
    enable = mkBoolOpt false "Whether or not to enable development role.";
  };

  config = mkIf cfg.enable {
    batteries = {
      tools = {
        nix-index = enabled;
        sshgpg = enabled;
      };

      cli = {
        bat = enabled;
        clipse = enabled;
        eza = enabled;
        fzf = enabled;
        htop = enabled;
        tools = enabled;
        network-tools = enabled;
        starship = enabled;
        zoxide = enabled;
        podman = enabled;
        development = enabled;
      };

      editors = {
        emacs = enabled;
      };
    };
  };
}

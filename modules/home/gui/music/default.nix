{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.batteries.gui.music;
in {
  options.batteries.gui.music = {
    enable = mkEnableOption "Enable music oriented programs";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      youtube-music
    ];
  };
}

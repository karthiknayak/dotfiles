{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.batteries.gui.social.discord;
in {
  options.batteries.gui.social.discord = {
    enable = mkEnableOption "Enable discord desktop app";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      discord
    ];
  };
}

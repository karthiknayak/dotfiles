{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.batteries.gui.programs;
in {
  options.batteries.gui.programs = {
    enable = mkEnableOption "Enable gnome adwaita GUI applications";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      mtpfs
      jmtpfs
      brightnessctl
      xdg-utils
      wl-clipboard
      pamixer
      playerctl

      grim
      slurp
      swappy
      pkgs.satty
      shotwell

      foliate
      pavucontrol
      pwvucontrol
      networkmanagerapplet

      sushi
      vlc
      loupe
      localsend

      ffmpegthumbnailer # thumbnails
      gst_all_1.gst-libav # thumbnails
    ];

    xdg.configFile."com.github.johnfactotum.Foliate/themes/mocha.json".text = ''
      {
          "label": "Mocha",
          "light": {
          	"fg": "#999999",
          	"bg": "#cccccc",
          	"link": "#666666"
          },
          "dark": {
          	"fg": "#cdd6f4",
          	"bg": "#1e1e2e",
          	"link": "#E0DCF5"
          }
      }
    '';
  };
}

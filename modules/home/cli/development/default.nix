{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.cli.development;
in {
  options.batteries.cli.development = with types; {
    enable = mkBoolOpt false "Whether or not to enable development tools";
  };

  config = mkIf cfg.enable {
    programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    home.packages = with pkgs; [
      # man
      man-pages
      man-pages-posix

      # git
      gnumake
      ninja
      meson
      (hiPrio gcc)
      clang
      clang-tools
      gdb
      curl
      openssl
      pkg-config

      # go
      go
      gopls

      # rust
      rust-analyzer

      bash-language-server
      yaml-language-server
      nodePackages_latest.vscode-json-languageserver
      nil
    ];

    home.file.gdbinit = {
      target = ".gdbinit";
      text = ''
      set confirm off
      set verbose off

      set auto-load safe-path /
      set debuginfod enabled on

      set history save on
      set history filename ~/.local/share/gdb/history

      set disassembly-flavor intel
      set print pretty on
      set print array on
    '';
    };
  };
}

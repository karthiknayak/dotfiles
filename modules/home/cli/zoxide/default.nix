{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.cli.zoxide;
in {
  options.batteries.cli.zoxide = with types; {
    enable = mkBoolOpt false "Whether or not to enable zoxide";
  };

  config = mkIf cfg.enable {
    programs.zoxide = {
      enable = true;
      enableFishIntegration = true;
    };
  };
}

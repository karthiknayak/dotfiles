{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.cli.clipse;
in {
  options.batteries.cli.clipse = with types; {
    enable = mkBoolOpt false "Whether or not to enable clipse";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      clipse
    ];

    home.file.".config/clipse/config.json" = {
      source = ./clipse.json;
      force = true;
    };
  };
}

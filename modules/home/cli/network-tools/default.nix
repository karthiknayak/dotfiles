{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.cli.network-tools;
in {
  options.batteries.cli.network-tools = with types; {
    enable = mkBoolOpt false "Whether or not to enable network tools";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      tshark
      termshark
      kubeshark
    ];
  };
}

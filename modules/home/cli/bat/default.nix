{
  config,
  lib,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.cli.bat;
in {
  options.batteries.cli.bat = with types; {
    enable = mkBoolOpt false "Whether or not to enable bat";
  };

  config = mkIf cfg.enable {
    programs.bat = {
      enable = true;
    };
  };
}

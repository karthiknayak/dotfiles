{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.cli.tools;
in {
  options.batteries.cli.tools = with types; {
    enable = mkBoolOpt false "Whether or not to enable cli tools";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      file
      broot
      choose
      curlie
      chafa
      dogdns
      doggo
      duf
      delta
      du-dust
      dysk
      entr
      erdtree
      fd
      gdu
      gping
      grex
      hyperfine
      hexyl
      jqp
      jnv
      ouch
      silver-searcher
      procs
      tokei
      trash-cli
      gtrash
      ripgrep
      sd
      xcp
      yq-go
      viddy
    ];
  };
}

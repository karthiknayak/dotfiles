{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.desktop.syncthing;
in {

  options.batteries.desktop.syncthing = with types; {
    enable = mkBoolOpt false "Whether to enable desktop Syncthing";
  };

  config = mkIf cfg.enable {
    services.syncthing = {
      enable = true;
    };
  };
}

{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.desktop.hypridle;
in {
  options.batteries.desktop.hypridle = with types; {
    enable = mkBoolOpt false "Whether to enable the hypridle";
  };

  config = mkIf cfg.enable {
    services.hypridle = {
      enable = true;
      settings = {
        general = {
          before_sleep_cmd = "loginctl lock-session";
          lock_cmd = "pidof hyprlock || ${pkgs.hyprlock}/bin/hyprlock";
          # unlock_cmd = "loginctl unlock-session";
          after_sleep_cmd = ''
          hyprctl dispatch dpms on && notify-send "Back from idle." "Welcome back!"'';
        };

        listener = [
          {
            timeout = 5;
            on-timeout = "pidof hyprlock && hyprctl dispatch dpms off";
            on-resume = "hyprctl dispatch dpms on";
          }
          {
            timeout = 150; # 2.5min.
            on-timeout =
              "${pkgs.light} -O && ${pkgs.light} -S 10"; # set monitor backlight to minimum, avoid 0 on OLED monitor.
            on-resume =
              "${pkgs.light} -I"; # monitor backlight restore.
          }
          {
            timeout = 300;
            on-timeout = "playerctl pause";
          }
          {
            timeout = 270;
            on-timeout = ''notify-send "Idle" "You're idle... locking in 30s."'';
          }
          {
            timeout = 300; # 5min
            on-timeout =
              "loginctl lock-session"; # lock screen when timeout has passed
          }

          {
            timeout = 330; # 5.5min
            on-timeout =
              "hyprctl dispatch dpms off"; # screen off when timeout has passed
            on-resume =
              "hyprctl dispatch dpms on"; # screen on when activity is detected after timeout has fired.
          }

          {
            timeout = 400;
            on-timeout = "systemctl suspend"; # suspend pc
          }
        ];
      };
    };
  };
}

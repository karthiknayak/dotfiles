{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.batteries.desktop.qt;
in {
  options.batteries.desktop.qt = {
    enable = mkEnableOption "enable qt theme management";
  };

  config = mkIf cfg.enable {
    qt = {
      enable = true;
      # platformTheme.name = "gtk";
      # style = {
      #   name = "adwaita-dark";
      #   package = pkgs.adwaita-qt;
      # };
    };
  };
}

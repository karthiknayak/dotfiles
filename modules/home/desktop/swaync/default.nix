{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.batteries.desktop.swaync;
in {
  options.batteries.desktop.swaync = {
    enable = mkEnableOption "Enable sway notification center";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      libnotify
    ];

    stylix.targets.swaync.enable = false;

    services.swaync = {
      enable = true;
      settings = {
        widgets = [
          "buttons-grid"
          "backlight"
          "volume"
          "mpris"
          "title"
          "dnd"
          "notifications"
        ];

        widget-config = {
          title = {
            text = "Notifications";
            clear-all-button = true;
            button-text = "Clear All";
          };

          dnd = {
            text = "Do Not Disturb";
          };

          label = {
            max-lines = 1;
            text = "Controll Center";
          };

          mpris = {
            image-size = 96;
            image-radius = 12;
          };

          backlight = {
            label = "󰃞";
            device = "intel_backlight";
            min = 10;
          };

          volume = {
            label = "";
          };

          buttons-grid = {
            actions = [
              {
                label = "";
                command = "nm-connection-editor";
              }
              {
                label = "󰂯";
                command = "blueman-manager";
              }
            ];
          };
        };
      };
      style = builtins.readFile ./swaync.css;
    };
  };
}

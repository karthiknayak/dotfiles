{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.batteries.desktop.hyprland;

  laptop_lid_switch = pkgs.writeShellScriptBin "laptop_lid_switch" ''
    #!/usr/bin/env bash

    if grep open /proc/acpi/button/lid/LID0/state; then
    		hyprctl keyword monitor "eDP-1, 1920x1200@59.95000, 0x0, 1"
    else
    		if [[ `hyprctl monitors | grep "Monitor" | wc -l` != 1 ]]; then
    				hyprctl keyword monitor "eDP-1, disable"
    		else
    				systemctl suspend
    		fi
    fi
  '';

    resize = pkgs.writeShellScriptBin "resize" ''
    #!/usr/bin/env bash

    # Initially inspired by https://github.com/exoess

    # Getting some information about the current window
    # windowinfo=$(hyprctl activewindow) removes the newlines and won't work with grep
    hyprctl activewindow > /tmp/windowinfo
    windowinfo=/tmp/windowinfo

    # Run slurp to get position and size
    if ! slurp=$(slurp); then
    		exit
    fi

    # Parse the output
    pos_x=$(echo $slurp | cut -d " " -f 1 | cut -d , -f 1)
    pos_y=$(echo $slurp | cut -d " " -f 1 | cut -d , -f 2)
    size_x=$(echo $slurp | cut -d " " -f 2 | cut -d x -f 1)
    size_y=$(echo $slurp | cut -d " " -f 2 | cut -d x -f 2)

    # Keep the aspect ratio intact for PiP
    if grep "title: Picture-in-Picture" $windowinfo; then
    		old_size=$(grep "size: " $windowinfo | cut -d " " -f 2)
    		old_size_x=$(echo $old_size | cut -d , -f 1)
    		old_size_y=$(echo $old_size | cut -d , -f 2)

    		size_x=$(((old_size_x * size_y + old_size_y / 2) / old_size_y))
    		echo $old_size_x $old_size_y $size_x $size_y
    fi

    # Resize and move the (now) floating window
    grep "fullscreen: 1" $windowinfo && hyprctl dispatch fullscreen
    grep "floating: 0" $windowinfo && hyprctl dispatch togglefloating
    hyprctl dispatch moveactive exact $pos_x $pos_y
    hyprctl dispatch resizeactive exact $size_x $size_y
  '';
in {
  config = mkIf cfg.enable {
    wayland.windowManager.hyprland = {
      settings = {
        input = {
          kb_layout = "us";
          kb_options = "ctrl:nocaps";
        };

        general = {
          gaps_in = 3;
          gaps_out = 5;
          border_size = 1;

          allow_tearing = false;
          resize_on_border = true;
        };

        decoration = {
          rounding = 5;
        };

        debug = {
          disable_logs = false;
        };

        group = {
          groupbar = {
            height = 1;
            render_titles = false;
            gradients = false;
          };
        };

        misc = {
          key_press_enables_dpms = true;
          mouse_move_enables_dpms = true;

          disable_autoreload = true;
          force_default_wallpaper = 0;
          animate_mouse_windowdragging = false;
          vrr = 1;
          focus_on_activate = true;
        };

        gestures = {
          workspace_swipe = true;
          workspace_swipe_distance = 200;
        };

        exec-once = [
          "dbus-update-activation-environment --systemd --all"
          "systemctl --user import-environment QT_QPA_PLATFORMTHEME"
          "${pkgs.clipse}/bin/clipse -listen"
        ];

        windowrulev2 = [
          "float,class:(clipse)" # ensure you have a floating window class set if you want this behavior
          "size 622 652,class:(clipse)" # set the size of the window as necessary
        ];

        binds = { allow_workspace_cycles = true; };

        "$mod" = "SUPER";

        bind = [
          # Launchers
          "$mod, return, exec, ${pkgs.alacritty}/bin/alacritty"
          "$mod, D,      exec, rofi -show drun -mode drun"
          "$mod, Z,      exec, rofi -show window"
          "$mod, slash,  exec, dunstctl close"
          "$mod, period, exec, dunstctl action"
          "$mod, grave,  exec, dunstctl history-pop"

          # Resize
          "SUPER, R, exec, ${resize}/bin/resize"

          # Clipboard
          "$mod, V, exec, alacritty -o \"font.size=12\" -o \"window.padding.x=10\" -o \"window.padding.y=10\" --class clipse -e 'clipse'"

          # Print screen
          ''$mod SHIFT, P, exec, grim -g "$(slurp)" - | swappy -f -''

          # Lock screen
          "$mod SHIFT, O, exec, ${pkgs.hyprlock}/bin/hyprlock"

          # Window management
          "$mod SHIFT, E,     exit"
          "$mod SHIFT, Q,     killactive,    0"
          "$mod,       F,     fullscreen,    0"
          "$mod,       F,     exec,          notify-send 'Fullscreen Mode'"
          "$mod,       P,     pseudo,"
          "$mod,       P,     exec,          notify-send 'Pseudo Mode'"
          "$mod,       SPACE, togglefloating,"
          "$mod,       SPACE, centerwindow,"
          "$mod,       TAB,   workspace,      previous"
          "$mod,       J,     togglesplit,"
          "$mod,       left,  movefocus,      l"
          "$mod,       right, movefocus,      r"
          "$mod,       up,    movefocus,      u"
          "$mod,       down,  movefocus,      d"

          # Special workspace
          "$mod, S, togglespecialworkspace"
          "$mod SHIFT, S, movetoworkspacesilent, special"

          # Tabbed
          "$mod, W, togglegroup"
          "$mod, E, changegroupactive"
          "$mod, h, changegroupactive, b"
          "$mod, l, changegroupactive, f"

          # Move window focus with vim keys.
          "$mod, h, movefocus, l"
          "$mod, l, movefocus, r"
          "$mod, k, movefocus, u"
          "$mod, j, movefocus, d"

          # Move into group
          "$mod SHIFT, h, movewindoworgroup, l"
          "$mod SHIFT, l, movewindoworgroup, r"
          "$mod SHIFT, k, movewindoworgroup, u"
          "$mod SHIFT, j, movewindoworgroup, d"

          # Workspaces
          "$mod, 1, workspace, 1"
          "$mod, 2, workspace, 2"
          "$mod, 3, workspace, 3"
          "$mod, 4, workspace, 4"
          "$mod, 5, workspace, 5"
          "$mod, 6, workspace, 6"
          "$mod, 7, workspace, 7"
          "$mod, 8, workspace, 8"
          "$mod, 9, workspace, 9"
          "$mod, 0, workspace, 10"

          # Move active window to a workspace
          "$mod SHIFT, 1, movetoworkspace, 1"
          "$mod SHIFT, 2, movetoworkspace, 2"
          "$mod SHIFT, 3, movetoworkspace, 3"
          "$mod SHIFT, 4, movetoworkspace, 4"
          "$mod SHIFT, 5, movetoworkspace, 5"
          "$mod SHIFT, 6, movetoworkspace, 6"
          "$mod SHIFT, 7, movetoworkspace, 7"
          "$mod SHIFT, 8, movetoworkspace, 8"
          "$mod SHIFT, 9, movetoworkspace, 9"
          "$mod SHIFT, 0, movetoworkspace, 10"

          # Scroll through existing workspaces
          "$mod, mouse_down, workspace, e+1"
          "$mod, mouse_up, workspace, e-1"
        ];

        bindl = [
          ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
          ", XF86AudioPlay, exec, playerctl play-pause"
          ", XF86AudioNext, exec, playerctl next"
          ", XF86AudioPrev, exec, playerctl previous"
          # ", switch:Lid Switch, exec, ${laptop_lid_switch}/bin/laptop_lid_switch"
        ];

        bindle = [
          ", XF86AudioRaiseVolume, exec, pamixer pamixer -i 5"
          ", XF86AudioLowerVolume, exec, pamixer pamixer -d 5"
        ];
      };
    };
  };
}

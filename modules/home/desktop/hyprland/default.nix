{
  lib,
  config,
  pkgs,
  namespace,
  inputs,
  ...
}:
let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.batteries) mkOpt enabled;

  cfg = config.batteries.desktop.hyprland;
in
{
  imports = with inputs;
    lib.snowfall.fs.get-non-default-nix-files ./.;

  options.batteries.desktop.hyprland = {
    enable = mkEnableOption "Enable hyprland and associated software";
  };

  config = mkIf cfg.enable {
    nix.settings = {
      trusted-substituters = ["https://hyprland.cachix.org"];
      trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
    };

    wayland.windowManager.hyprland = {
      enable = true;
      # set the flake package
      package = inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;
      systemd.enable = true;
      xwayland.enable = true;
    };

    batteries.desktop = {
      hyprlock = enabled;
      hypridle = enabled;
      kanshi = enabled;
      waybar = enabled;
      rofi = enabled;
      swaync = enabled;
      gtk = enabled;
      qt = enabled;
    };
  };
}

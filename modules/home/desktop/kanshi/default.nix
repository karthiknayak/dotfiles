{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.desktop.kanshi;
in {
  options.batteries.desktop.kanshi = with types; {
    enable = mkBoolOpt false "Whether to enable Kanshi";
  };

  config = mkIf cfg.enable {
    services.kanshi = {
      enable = true;
      systemdTarget = "graphical-session.target";
      settings = [
        {
          profile.name = "undocked";
          profile.outputs = [
            {
              criteria = "eDP-1";
              status = "enable";
              position = "0,0";
              scale = 1.0;
            }
          ];
        }
        {
          profile.name = "docked";
          profile.outputs = [
            {
              criteria = "*";
              position = "0,0";
            }
            {
              criteria = "eDP-1";
              status = "disable";
            }
          ];
        }
      ];
    };
  };
}

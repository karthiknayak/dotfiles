;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; https://manytools.org/hacker-tools/ascii-banner/ (JS Stick Letters)

;;       __   ___  __
;; |  | /__` |__  |__)
;; \__/ .__/ |___ |  \
;;
(setq user-full-name "Karthik Nayak"
      user-mail-address "karthik.188@gmail.com")

;;  __   ___       ___  __
;; / _` |__  |\ | |__  |__)  /\  |
;; \__> |___ | \| |___ |  \ /~~\ |___
;;
;; Font
(setq doom-font (font-spec :size 18 ))
;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)
;; Update elfeed to automatically load new articles.
(add-hook 'elfeed-search-mode-hook #'elfeed-update)
;; By default select the first preview
(after! corfu
  (setq corfu-preselect t))
(global-set-key (kbd "C-c g l") 'git-link)

;;  __
;; /  `
;; \__,
;;
;; Disable formatting on certain modes, default + c-mode
(setq +format-on-save-disabled-modes
      '(emacs-lisp-mode  ; elisp's mechanisms are good enough
        sql-mode         ; sqlformat is currently broken
        tex-mode         ; latexindent is broken
        latex-mode
        c-mode))
(defun my-c-mode-hook ()
  (setq c-default-style "linux"
        c-basic-offset 8
        indent-tabs-mode t))
(add-hook 'c-mode-hook 'my-c-mode-hook)
(after! dap-mode
  (require 'dap-cpptools))

;;  __   __                  __
;; / _` /  \ |     /\  |\ | / _`
;; \__> \__/ |___ /~~\ | \| \__>
;;
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
(after! lsp-mode
  (setq  lsp-go-analyses '((shadow . t)
                           (unusedvariable . t)
                           (useany . t)))
  (setq  lsp-go-use-gofumpt t)
  )

;;  ___
;; |__   |\/|  /\  | |
;; |___  |  | /~~\ | |___
;;
(after! notmuch
  (setq  notmuch-saved-searches
         '((:name "inbox" :query "tag:inbox not tag:trash" :key "i")
           (:name "flagged" :query "tag:flagged" :key "f")
           (:name "sent" :query "tag:sent" :key "s")
           (:name "drafts" :query "tag:draft" :key "d")
           (:name "git [all]" :query "tag:Git" :key "ga")
           (:name "git [me]" :query "tag:Git and to:karthik.188@gmail.com" :key "gm")))
  )
(setq sendmail-program "gmi")
(setq message-sendmail-extra-arguments '("send" "--quiet" "-t" "-C" "~/mail/personal"))
(setq notmuch-fcc-dirs nil)
(setq +notmuch-mail-folder "~/mail/personal")
(add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)

;;  __   __   __
;; /  \ |__) / _`
;; \__/ |  \ \__>
;;
(setq org-directory "~/Sync/org/")
;; GTD
(setq org-agenda-files (list "~/Sync/org/gtd/gtd.org"
                             "~/Sync/org/gtd/tickler.org"
                             "~/Sync/org/gtd/inbox.org"  ))
(setq org-refile-targets '(("~/Sync/org/gtd/gtd.org"     :maxlevel . 4)
                           ("~/Sync/org/gtd/someday.org" :level    . 2)
                           ("~/Sync/org/gtd/tickler.org" :maxlevel . 1)))
(setq org-agenda-text-search-extra-files (list "~/Sync/org/gtd/inbox.org_archive"
                                               "~/Sync/org/gtd/gtd.org_archive"
                                               "~/Sync/org/gtd/tickler.org_archive"))
;; Custom Configurations
(after! org
  (setq org-startup-folded 'show2levels)
  (setq org-capture-templates '(("t" "Todo [inbox]" entry
                                 (file+headline "~/Sync/org/gtd/inbox.org" "Tasks")
                                 "* TODO %i%?")
                                ("T" "Tickler" entry
                                 (file+headline "~/Sync/org/gtd/tickler.org" "Tickler")
                                 "* %i%? \n %U"))))
(add-hook 'auto-save-hook 'org-save-all-org-buffers)
(add-to-list 'org-modules 'org-habit t)
(setq org-startup-folded t)
(setq org-roam-directory "~/Sync/org/notes")
;; Run M-x org-id-update-id-locations and org-roam-update-org-id-locations
(setq org-id-track-globally t)
;; Org alerts
(after! org
  (org-wild-notifier-mode)
  (setq org-wild-notifier-alert-time '(60 30)))
;; GitHub markdown export
(eval-after-load "org"
  '(require 'ox-gfm nil t))

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

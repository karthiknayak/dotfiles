{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.editors.emacs;
  emacs = with pkgs; (emacsPackagesFor emacs-pgtk).emacsWithPackages (epkgs: with epkgs; [
    treesit-grammars.with-all-grammars
    vterm
  ]);
in {
  options.batteries.editors.emacs = with types; {
    enable = mkBoolOpt false "Whether or not to enable emacs";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      binutils
      emacs

      # Doom dependencies
      git
      ripgrep
      gnutls

      # Optional dependencies
      fd                  # faster projectile indexing
      imagemagick         # for image-dired
      zstd                # for undo-fu-session/undo-tree compression

      ## Module dependencies
      # :checkers spell
      (aspellWithDicts (ds: with ds; [ en en-computers en-science ]))
      # :tools editorconfig
      editorconfig-core-c # per-project style config
      # :tools lookup & :lang org +roam
      sqlite
      # :lang cc
      clang-tools
      # :lang latex & :lang org (latex previews)
      texlive.combined.scheme-medium
      # :lang nix
      age
    ];

    home.file.".config/doom" = {
      source = ./doom.d;
      recursive = true;
      force = true;
      onChange =
        "~/.config/emacs/bin/doom sync && ~/.config/emacs/bin/doom env --allow-all";
    };
  };
}

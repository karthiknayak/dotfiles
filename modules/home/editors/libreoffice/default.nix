{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.editors.libreoffice;
in {
  options.batteries.editors.libreoffice = with types; {
    enable = mkBoolOpt false "Whether or not to enable libreoffice";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      libreoffice-qt
      hunspell
    ];
  };
}

{
  config,
  pkgs,
  lib,
  ...
}:
with lib;
with lib.batteries; let
  cfg = config.batteries.services.kdeconnect;
in {
  options.batteries.services.kdeconnect = with types; {
    enable = mkBoolOpt false "Whether or not to manage kdeconnect";
  };

  config = mkIf cfg.enable {
    services.kdeconnect = {
      enable = true;
      indicator = true;
    };
  };
}

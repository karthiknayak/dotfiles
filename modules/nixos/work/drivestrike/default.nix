{
  lib,
  pkgs,
  config,
  namespace,
  ...
}:
with lib;
with lib.${namespace}; let
  cfg = config.${namespace}.work.drivestrike;
in {
  options.${namespace}.work.drivestrike = with types; {
    enable = mkBoolOpt false "Whether or not to enable drivestrike.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.batteries.drivestrike
    ];

    systemd.services.drivestrike = {
      enable = true;
      description = "DriveStrike Client Service";
      wantedBy = [ "multi-user.target" ];

      path = [
        pkgs.dmidecode
        pkgs.glib-networking
        pkgs.batteries.drivestrike
      ];

      unitConfig = {
        Description = "DriveStrike Client Service";
        After = [
          "network.target"
          "drivestrike-lock.service"
        ];
      };

      serviceConfig = {
        Type = "simple";
        Restart = "always";
        RestartSec = "10";
        ExecStart = "${pkgs.batteries.drivestrike}/bin/drivestrike run";
        SyslogIdentifier = "drivestrike";
      };
    };
  };
}

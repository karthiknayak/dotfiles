{
  options,
  config,
  pkgs,
  lib,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.user;
in
{
  options.${namespace}.user = with types; {
    name = mkOpt str "karthik" "The name to use for the user account.";
    initialPassword = mkOpt str "1" "The initial password to use";
    extraGroups = mkOpt (listOf str) [ ] "Groups for the user to be assigned.";
  };

  config = {
    programs.fish = {
      enable = true;
    };

    users.users.${cfg.name} = {
      isNormalUser = true;
      inherit (cfg) name initialPassword;
      home = "/home/${cfg.name}";
      group = "users";

      shell = pkgs.fish;

      extraGroups = [
        "wheel"
        "audio"
        "sound"
        "video"
        "vboxusers"
      ] ++ cfg.extraGroups;
    };
  };
}

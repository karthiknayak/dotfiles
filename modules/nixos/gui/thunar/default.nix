{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.batteries.gui.thunar;
in {
  options.batteries.gui.thunar = {
    enable = mkEnableOption "Enable Thunar";
  };

  config = mkIf cfg.enable {
    programs.thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        thunar-archive-plugin
        thunar-volman
        thunar-media-tags-plugin
      ];
    };
    services.gvfs.enable = true;
    services.tumbler.enable = true;

    environment = {
      systemPackages = with pkgs; [

        # required by tumbler service
        # TODO: add https://gitlab.com/hxss-linux/folderpreview
        ffmpegthumbnailer # videos
        freetype # fonts
        gdk-pixbuf # images
        libgepub # .epub
        libgsf # .odf
        poppler # .pdf .ps
        webp-pixbuf-loader # .webp
        file-roller
        kdePackages.ark
        xarchiver
      ];
    };
  };
}

{
  config,
  lib,
  namespace,
  ...
}:
with lib; let
  cfg = config.${namespace}.services.virtualisation.podman;
in {
  options.${namespace}.services.virtualisation.podman = {
    enable = mkEnableOption "Enable podman";
  };

  config = mkIf cfg.enable {
    batteries.user.extraGroups = [ "docker" ];

    virtualisation = {
      docker.enable = true;
    };
  };
}

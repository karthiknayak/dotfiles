{
  config,
  lib,
  namespace,
  ...
}:
with lib; let
  cfg = config.${namespace}.services.fwupd;
in {
  options.${namespace}.services.fwupd = {
    enable = mkEnableOption "Enable fwupd";
  };

  config = mkIf cfg.enable {
    services.fwupd.enable = true;
  };
}

{
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.services.work.falcon-sensor;
  falcon = pkgs.${namespace}.falcon-sensor;
in
{
  options.${namespace}.services.work.falcon-sensor = {
    enable = mkBoolOpt false "Whether or not to enable falcon sensor.";
    cid_path = lib.mkOption { type = lib.types.str; };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [
      falcon.pkgs.falconctl
      falcon.pkgs.falcon-kernel-check
    ];

    systemd.services.falcon-sensor = {
      enable = true;
      description = "CrowdStrike Falcon Sensor";
      unitConfig.DefaultDependencies = false;
      after = [ "local-fs.target" ];
      conflicts = [ "shutdown.target" ];
      before = [
        "sysinit.target"
        "shutdown.target"
      ];
      serviceConfig = {
        ExecStartPre =
          (pkgs.writeShellScriptBin "init-falcon" ''
            mkdir -p /opt/CrowdStrike
            cid=$(cat ${cfg.cid_path})
            ln -sf ${falcon}/opt/CrowdStrike/* /opt/CrowdStrike
            ${falcon.pkgs.falconctl}/bin/falconctl -f -s --cid=$cid
          '')
          + "/bin/init-falcon";
        ExecStart = "${falcon.pkgs.falcond}/bin/falcond";
        Type = "forking";
        PIDFile = "/run/falcond.pid";
        Restart = "no";
        TimeoutStopSec = "60s";
        KillMode = "process";
      };
      wantedBy = [ "multi-user.target" ];
    };
  };
}

{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.roles.common;
in
{
  options.${namespace}.roles.common = with types; {
    enable = mkBoolOpt false "Whether or not to enable common configuration.";
  };

  config = mkIf cfg.enable {
    batteries = {
      system = {
        boot = enabled;
        time = enabled;
        locale = enabled;
        xkb = enabled;
        nix = enabled;
        fonts = enabled;
      };

      security = {
        sops = enabled;
      };

      hardware = {
        networking = enabled;
        bluetooth = enabled;
        audio = enabled;
        mouse = enabled;
      };
    };
  };
}

{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.roles.work;
in
{
  options.${namespace}.roles.work = with types; {
    enable = mkBoolOpt false "Whether or not to enable work configuration.";
  };

  config = mkIf cfg.enable {
    sops.secrets.falcon_cid = {
      sopsFile = ../../../../secrets/work.yaml;
    };

    batteries = {
      services.work.falcon-sensor = {
        enable = true;
        cid_path = "${config.sops.secrets.falcon_cid.path}";
      };
    };

    services.teleport.enable = true;
  };
}

{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.roles.development;
in
{
  options.${namespace}.roles.development = with types; {
    enable = mkBoolOpt false "Whether or not to enable development configuration.";
  };

  config = mkIf cfg.enable {

    environment = {
      systemPackages = with pkgs; [
        cargo-flamegraph
        valgrind
      ];
    };

    batteries = {
      services = {
        virtualisation.podman.enable = true;
        fwupd.enable = true;
      };
    };
  };
}

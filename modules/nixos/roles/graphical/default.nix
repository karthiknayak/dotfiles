{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.roles.graphical;
in
{
  options.${namespace}.roles.graphical = with types; {
    enable = mkBoolOpt false "Whether or not to enable graphical configuration.";
  };

  config = mkIf cfg.enable {
    programs.light = {
      enable = true;
      brightnessKeys = {
        enable = true;
      };
    };

    batteries = {
      desktop = {
        hyprland = enabled;
        styling = enabled;
      };

      gui = {
        thunar = enabled;
      };
    };
  };
}

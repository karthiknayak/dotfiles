{
  options,
  config,
  pkgs,
  lib,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.hardware.networking;
in
{
  options.${namespace}.hardware.networking = with types; {
    enable = mkBoolOpt false "Whether or not to enable networking support";
    hosts = mkOpt attrs { } (mdDoc "An attribute set to merge with `networking.hosts`");
  };

  config = mkIf cfg.enable {
    batteries.user.extraGroups = [ "networkmanager" ];

    networking = {
      firewall = {
        enable = true;
        allowedTCPPortRanges = [
          {
            from = 1714;
            to = 1764;
          }
          # localsend
          {
            from = 53317;
            to = 53317;
          }
        ];
        allowedUDPPortRanges = [
          {
            from = 1714;
            to = 1764;
          }
          # localsend
          {
            from = 53317;
            to = 53317;
          }
        ];
      };
      networkmanager = {
        enable = true;
      };
      hosts = cfg.hosts;
    };

    # Fixes an issue that normally causes nixos-rebuild to fail.
    # https://github.com/NixOS/nixpkgs/issues/180175
    systemd.services.NetworkManager-wait-online.enable = false;
  };
}

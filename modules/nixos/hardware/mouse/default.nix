{
  config,
  lib,
  namespace,
  ...
}:
with lib; let
  cfg = config.${namespace}.hardware.mouse;
in {
  options.${namespace}.hardware.mouse = {
    enable = mkEnableOption "Enable wakeup with mouse";
  };

  config = mkIf cfg.enable {
    services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="062a", ATTR{idProduct}=="4102", TEST=="power/wakeup", ATTR{power/wakeup}="enabled"
    ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="046d", ATTR{idProduct}=="c52b", TEST=="power/wakeup", ATTR{power/wakeup}="enabled"
  '';
  };
}

{
  options,
  config,
  pkgs,
  lib,
  namespace,
  inputs,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.desktop.greetd;
  user = config.${namespace}.user;
in
{
  options.${namespace}.desktop.greetd = with types; {
    enable = mkBoolOpt false "Whether or not to enable greetd";
  };

  config = mkIf cfg.enable {
    services.greetd = {
      enable = true;
      settings = rec {
        default_session = {
          command = "Hyprland &> /dev/null";
          user = user.name;
        };
        initial_session = default_session;
      };
    };
  };
}
